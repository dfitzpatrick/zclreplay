from setuptools import setup

setup(
    name='zclreplay',
    version='0.1',
    packages=['zclreplay'],
    install_requires=[
        'mpyq',
        's2protocol'
    ],
    url='https://gitlab.com/dfitzpatrick/zclreplay',
    license='MIT',
    author='Derek Fitzpatrick',
    author_email='dfitz.murrieta@gmail.com',
    description='Replay Parser for Starcraft II Arcade Map Zone Control CE'
)
